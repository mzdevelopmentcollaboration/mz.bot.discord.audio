using System;
using Xunit;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Mz.Bot.Discord.Audio.Tests
{
    public class MediaTests
    {
        [Fact]
        public async Task QueryMediaUrlAsync()
        {
            var youtubeDownloadProcess = Process.Start(new ProcessStartInfo
            {
                FileName = "youtube-dl",
                Arguments = $"-g \"https://www.youtube.com/watch?v=8b2MWZKslwU\"",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
            if (youtubeDownloadProcess != null)
            {
                await youtubeDownloadProcess
                    .WaitForExitAsync();
                var youtubeDownloadProcessOutput =
                    await youtubeDownloadProcess
                        .StandardOutput
                        .ReadToEndAsync();
                var parsedYoutubeDownloadProcessOutput =
                    youtubeDownloadProcessOutput.Split(
                        new[] {"\n"},
                        StringSplitOptions.None);
                parsedYoutubeDownloadProcessOutput
                    .Where(
                        x => !x.Equals(string.Empty) && x.Contains("mime=audio%2Fwebm"));
            }
        }
    }
}
