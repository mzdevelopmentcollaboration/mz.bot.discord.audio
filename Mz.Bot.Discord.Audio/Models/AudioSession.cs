﻿using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;

using Discord;
using Discord.Audio;

namespace Mz.Bot.Discord.Audio.Models
{
    public class AudioSession
    {
        public Stream AudioStream { get; set; }
        public Stream MediaStream { get; set; }
        public Process MediaProcess { get; set; }
        public IAudioClient AudioClient { get; set; }
        public IVoiceChannel VoiceChannel { get; set; }
        public Task MediaStreamTransmission { get; set; }
    }
}
