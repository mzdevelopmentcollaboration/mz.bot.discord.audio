﻿using Mz.Bot.Discord.Audio.Manager;

namespace Mz.Bot.Discord.Audio
{
    internal class Program
    {
        private static void Main() =>
            new DiscordManager().BotManagerAsync("PLACEHOLDER")
                .GetAwaiter().GetResult();
    }
}
