﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using Mz.Bot.Discord.Audio.Models;

namespace Mz.Bot.Discord.Audio.Manager
{
    internal class DiscordManager
    {
        private readonly CommandService
            _discordCommandService = new();
        private readonly List<AudioSession> _audioSessions =
            new();
        private readonly DiscordSocketClient
            _discordClient = new();
        private readonly IServiceProvider _serviceCollection;

        public DiscordManager()
        {
            _discordClient.Log += LogEvent;
            _discordClient.MessageReceived +=
                HandleCommandAsync;
            _serviceCollection = new ServiceCollection()
                .AddSingleton(_audioSessions)
                .BuildServiceProvider();
        }

        public async Task BotManagerAsync(string loginToken)
        {
            await _discordCommandService.AddModulesAsync(
                Assembly.GetEntryAssembly(), _serviceCollection);
            await _discordClient.LoginAsync(TokenType.Bot, loginToken);
            await _discordClient.StartAsync();
            await Task.Delay(-1);
        }

        private static Task LogEvent(LogMessage message)
        {
            Console.WriteLine(message.ToString());
            return Task.CompletedTask;
        }

        private async Task HandleCommandAsync(IDeletable socketMessage)
        {
            if (socketMessage is not SocketUserMessage
                message) return;
            var argumentPosition = 0;
            if (!(message.HasCharPrefix('!',
                      ref argumentPosition) ||
                  message.HasMentionPrefix(
                      _discordClient.CurrentUser,
                      ref argumentPosition)) ||
                message.Author.IsBot) return;
            var messageContext =
                new SocketCommandContext(_discordClient,
                    message);
            await _discordCommandService.ExecuteAsync(
                messageContext, argumentPosition,
                _serviceCollection);
        }
    }
}
