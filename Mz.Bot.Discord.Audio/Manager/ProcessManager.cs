﻿using System;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Mz.Bot.Discord.Audio.Manager
{
    internal class ProcessManager
    {
        public Process CreateMediaProcess(string path)
        {
            return Process.Start(new ProcessStartInfo
            {
                FileName = "ffmpeg",
                Arguments = $"-hide_banner -loglevel panic -i \"{path}\" -ac 2 -f s16le -ar 48000 pipe:1",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
        }

        public async Task<string> QueryMediaUrlAsync(Uri videoUrl)
        {
            try
            {

            var youtubeDownloadProcess = Process.Start(new ProcessStartInfo
            {
                FileName = "youtube-dl",
                Arguments = $"-g \"{videoUrl}\"",
                UseShellExecute = false,
                RedirectStandardOutput = true
            });
            if (youtubeDownloadProcess == null)
            {
                return string.Empty;
            }
            await youtubeDownloadProcess
                .WaitForExitAsync();
            var youtubeDownloadProcessOutput =
                await youtubeDownloadProcess
                    .StandardOutput
                    .ReadToEndAsync();
            youtubeDownloadProcess.Close();
            youtubeDownloadProcess.Dispose();
            var parsedYoutubeDownloadProcessOutput =
                youtubeDownloadProcessOutput.Split(
                    new[] { "\n" },
                    StringSplitOptions.None);
            return parsedYoutubeDownloadProcessOutput
                .First(x => !x.Equals(string.Empty) &&
                            x.Contains(
                                "mime=audio"));
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
