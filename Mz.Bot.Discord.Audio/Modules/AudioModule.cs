﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Discord;
using Discord.Audio;
using Discord.Commands;

using Mz.Bot.Discord.Audio.Models;
using Mz.Bot.Discord.Audio.Manager;

namespace Mz.Bot.Discord.Audio.Modules
{
    public class AudioModule : ModuleBase<SocketCommandContext>
    {
        private readonly ProcessManager _processManager;
        private readonly List<AudioSession> _audioSessions;
        private CancellationToken _cancellationToken;
        private CancellationTokenSource _cancellationTokenSource = new();

        public AudioModule(List<AudioSession> audioSessions)
        {
            _audioSessions = audioSessions;
            _processManager = new ProcessManager();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        [Command("join", RunMode = RunMode.Async)]
        public async Task JoinChannel(IVoiceChannel voiceChannel = null)
        {
            voiceChannel ??= (Context.User as IGuildUser)
                ?.VoiceChannel;
            if (voiceChannel != null)
            {
                _audioSessions.Add(new AudioSession()
                {
                    AudioClient = await voiceChannel.ConnectAsync(),
                    VoiceChannel = voiceChannel
                });
                return;
            }
            await Context.Channel.SendMessageAsync("USER IS NOT JOINED");
        }

        [Command("play", RunMode = RunMode.Async)]
        public async Task PlayAudio(string videoUrl,
            IVoiceChannel voiceChannel = null)
        {
            voiceChannel ??= (Context.User as IGuildUser)
                ?.VoiceChannel;
            var audioSession = _audioSessions.First(
                x => x.VoiceChannel.Equals(voiceChannel));
            if (audioSession != null)
            {
                if (audioSession.AudioClient
                    .ConnectionState.Equals(
                        ConnectionState.Connected))
                {
                    await SendAudioAsync(audioSession.AudioClient,
                        await _processManager.QueryMediaUrlAsync(
                            new Uri(videoUrl)));
                }
                else
                {
                    await audioSession
                        .VoiceChannel.ConnectAsync();
                    await SendAudioAsync(audioSession.AudioClient,
                        await _processManager.QueryMediaUrlAsync(
                            new Uri(videoUrl)));
                }
            }
        }

        [Command("stop")]
        public async Task StopAudio(IVoiceChannel voiceChannel = null)
        {
            voiceChannel ??= (Context.User as IGuildUser)
                ?.VoiceChannel;
            var audioSession = _audioSessions.First(
                x => x.VoiceChannel.Equals(voiceChannel));
            if (audioSession != null && audioSession.AudioClient
                .ConnectionState.Equals(ConnectionState.Connected))
            {
                // ReSharper disable once MethodSupportsCancellation
                await Task.Run(() => _cancellationTokenSource.Cancel());
            }
        }

        [Command("leave")]
        public async Task LeaveChannel(IVoiceChannel voiceChannel = null)
        {
            voiceChannel ??= (Context.User as IGuildUser)
                ?.VoiceChannel;
            var audioSession = _audioSessions.First(
                x => x.VoiceChannel.Equals(voiceChannel));
            if (audioSession != null && audioSession.AudioClient
                .ConnectionState.Equals(ConnectionState.Connected))
            {
                //TODO: Check why Cancellation only works through stopping
                await audioSession.AudioClient.StopAsync();
                _cancellationTokenSource.Cancel();
                _audioSessions.Remove(audioSession);
            }
        }

        private async Task SendAudioAsync(IAudioClient audioClient,
            string mediaPath)
        {
            using var mediaProcess = _processManager
                .CreateMediaProcess(mediaPath);
            await using var mediaStream = mediaProcess
                .StandardOutput.BaseStream;
            await using var discordStream = audioClient
                .CreatePCMStream(AudioApplication.Mixed);
            try
            {
                await mediaStream.CopyToAsync(discordStream,
                    _cancellationToken);
            }
            finally
            {
                mediaProcess.Kill(true);
                mediaProcess.Close();
                // ReSharper disable once MethodSupportsCancellation
                await mediaStream.FlushAsync();
                await mediaStream.DisposeAsync();
                // ReSharper disable once MethodSupportsCancellation
                await discordStream.FlushAsync();
            }
        }
    }
}
